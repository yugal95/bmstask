import axios from 'axios';

// const oConfigHeaders = {
// 	'Content-Type': 'application/json',
// 	crossDomain: true,
// 	credentials: 'cross-origin',
// 	'Access-Control-Allow-Origin': '*',
// 	'Access-Control-Allow-Headers': '*',
// 	'Access-Control-Allow-Methods': '*',
// };

// const oConfigOptions = {
// 	'Access-Control-Allow-Origin': '*',
// 	// 'Access-Control-Allow-Headers': '*',
// 	// 'Access-Control-Allow-Methods': '*',
// 	withCredentials: true,
// 	credentials: 'same-origin',
// };

/**
 * to update the localstorage with the new updated data
 * @param  {Object}   numbers  new data
 * @param  {Function} callback callback function
 */
export const updateLocalStorage = (numbers = {}, callback) => {
	localStorage.setItem('numbers', JSON.stringify(numbers));
	if (callback) {
		callback(numbers);
	}
};

/**
 * to get required data from localstorage of the browser
 * @return {Object} required data
 */
export const getLocalStorage = () => {
	return JSON.parse(localStorage.getItem('numbers') || '{}');
};

/**
 * to perform get API call and return the response
 * @param  {Sring} 		API_URI   API url, on which the request is need to be done
 * @param  {Function} fnSuccess success callback function
 * @param  {Function} fnError   error callback function
 */
export const getDataFromService = (API_URI, fnSuccess, fnError) => {
		axios.get(API_URI)
		.then(response => {
			fnSuccess(response && response.data);
		})
		.catch(({ error, response }) => {
			fnError(response, fnError);
		});
};
