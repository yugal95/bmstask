import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import {
	GridList,
	GridListTile,
	GridListTileBar,
	IconButton,
	Grid,
	Typography,
} from '@material-ui/core';
import { ThumbUpAlt, PlayCircleOutline } from '@material-ui/icons';
import { MOVIE_URI, BMS_IMAGE_API } from '../constants';
import Loader from '../components/Loader';
import DisplayVideo from '../components/DisplayVideo';
import { getDataFromService } from '../services';

const styles = theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		backgroundColor: '#282c34',
		// height: '100vh',
		// backgroundColor: theme.palette.background.paper,
	},
	gridList: {
		margin: '5%',
		justifyContent: 'center',
	},
	gridListTile: {
		width: '20%',
		margin: '1%',
		cursor: 'pointer',
		position: 'relative',
	},
	activeMovie: {
		background: '#e0dbd9',
	},
	videoBtn: {
		position: 'absolute',
		fontSize: theme.spacing.unit * 8,
		opacity: 0.8,
		color: '#fff',
		padding: '12px 24px',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%, -50%)',
		'-ms-transform': ' translate(-50%, -50%)',

		transition: 'all 0.3s',
		'&:hover': {
			fontSize: theme.spacing.unit * 10,
		},
	},
	listOverlay: {
		// transition: 'all 0.3s',
		// '&:hover': {
		// 	transform: 'scale(1.0)',
		// },
	},
	icon: {
		color: 'rgba(255, 255, 255, 0.54)',
	},
	img: {
		width: '100%',
		opacity: 0.9, // height: '100%',
		transition: 'all 0.3s',
		'&:hover': {
			// transform: 'scale(1.1)',
		},
	},
	panelSummary: {
		padding: 0,
	},
	video: {
		height: '300px',
		width: '100%',
	},
	success: {
		color: '#5a9900',
	},
	titleBar: {
		fontSize: '0.9em',
		marginLeft: 2,
		// width: '100%',
	},
	actionIcon: {
		fontSize: '0.9em',
		width: '40%',
		margin: 2,
	},
	titleWrap: {
		margin: 0,
	},
	titleBarRoot: {
		alignItems: 'flex-start',
		fontSize: '0.9em',
	},
	titleBarRootSubtitle: {
		height: 'auto',
		padding: '5px',
	},
	noPadding: {
		padding: 0,
	},
	percent: { margin: '2px', color: '#fff', textAlign: 'right' },
});

class Task2 extends Component {
	state = {
		loading: false,
		data: [],
		movies: [],
		languages: [],
		error_msg: '',
		selectedMovie: {},
		open: false,
		anchorEl: null,
		finalIndex: 0,
	};

	componentDidMount() {
		this.setState({
			loading: true,
		});
		getDataFromService(
			MOVIE_URI,
			response => {
				const languageList = response[0];
				const movieList = Object.values(response[1] || {});
				this.setState({
					movies: movieList,
					data: movieList,
					languages: languageList,
					loading: false,
				});
			},
			error => {
				this.setState({
					loading: false,
					error_msg: 'Could not fetch the desired movie list',
					data: [],
				});
			},
		);
	}

	handleMovieClick = (movie, index) => event => {
		const { currentTarget } = event;
		this.calculateLIsInRow(currentTarget, index, movie);
	};

	calculateLIsInRow(currentTarget, index, selectedMovie) {
		const { movies } = this.state;

		const lists = document.querySelectorAll('#movieList li');

		let currentIndex = index;
		let finalIndex = 0;
		for (let i = currentIndex; i > 0; i--) {
			if (
				lists[i] &&
				lists[i - 1] &&
				lists[i].getBoundingClientRect().top !==
					lists[i - 1].getBoundingClientRect().top
			) {
				finalIndex = i;
				break;
			}
		}
		const newMovies = Object.assign([], movies);
		newMovies.splice(finalIndex, 0, {
			...movies[index],
			showVideo: true,
		});
		this.setState({
			data: newMovies,
			finalIndex: finalIndex,
			anchorEl: lists[finalIndex],
			selectedMovie: selectedMovie,
		});
	}

	render() {
		const { classes } = this.props;
		const { loading, data, anchorEl, selectedMovie } = this.state;

		return (
			<div className={classes.root}>
				<Loader loading={loading} />
				<GridList
					id="movieList"
					cellHeight={'auto'}
					className={classes.gridList}
					spacing={8}
				>
					{data.map((movie, index) => {
						return movie.showVideo ? (
							<DisplayVideo
								key={`video${movie.EventCode}`}
								anchorEl={anchorEl}
								className={classes.videoTile}
								movie={selectedMovie}
							/>
						) : (
							<GridListTile
								key={movie.EventCode}
								style={{
									width: 205,
									height: 280,
								}}
								id={'list' + movie.EventCode}
								className={classNames(
									classes.gridListTile,
									selectedMovie.EventCode === movie.EventCode &&
										classes.activeMovie,
								)}
								onClick={this.handleMovieClick(movie, index)}
							>
								<div>
									<div className={classes.listOverlay}>
										<img
											src={`${BMS_IMAGE_API}${movie.EventCode}.jpg`}
											alt={movie.EventName}
											className={classes.img}
										/>
										<PlayCircleOutline
											className={classNames(classes.videoBtn)}
										/>
									</div>
									<GridListTileBar
										classes={{
											title: classes.titleBar,
											actionIcon: classes.actionIcon,
											root: classes.titleBarRoot,
											rootSubtitle: classes.titleBarRootSubtitle,
											titleWrap: classes.titleWrap,
										}}
										title={movie.EventTitle}
										actionIcon={
											<Grid container direction="column">
												<Grid item>
													<Grid
														container
														alignItems="center"
														justify="flex-end"
													>
														<Grid item>
															<IconButton
																className={classNames(
																	classes.success,
																	classes.noPadding,
																)}
															>
																<ThumbUpAlt />
															</IconButton>
														</Grid>
														<Grid item>
															<Typography
																color="inherit"
																variant="caption"
																className={classes.percent}
															>
																{movie.wtsPerc}%
															</Typography>
														</Grid>
													</Grid>
												</Grid>
												<Grid item>
													<Typography
														variant="caption"
														color="inherit"
														className={classes.percent}
													>
														{movie.csCount} votes
													</Typography>
												</Grid>
											</Grid>
										}
									/>
								</div>
							</GridListTile>
						);
					})}
				</GridList>
			</div>
		);
	}
}

Task2.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Task2);
