import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import {
  TextField,
  Grid,
  Button,
  Typography,
  Tooltip,
} from '@material-ui/core';
import { Info, CheckCircle, Clear } from '@material-ui/icons';
import { updateLocalStorage, getLocalStorage } from '../services';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  button: {
    margin: theme.spacing.unit,
  },
  success: {
    color: '#5a9900',
  },
  info: {
    color: '#461bbf',
  },
  error: {
    color: '#e9626d',
  },
  text: {
    width: '100%',
  },
  pointer: {
    cursor: 'pointer',
  },
  extraPadding: {
    [theme.breakpoints.down('xs')]: {
      padding: '2%',
    },
  },
  list: {
    maxWidth: '50%',
    [theme.breakpoints.down('xs')]: {
      padding: '2%',
      maxWidth: '100%',
    },
  },
});

class Task1 extends Component {
  state = {
    input: '',
    numbers: {},
    repeatedList: {},
    insertedList: [],
    successText: '',
    infoText: '',
  };

  componentDidMount() {
    this.setState({
      numbers: getLocalStorage(),
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    nextState.numbers = getLocalStorage();
    return true;
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  clearList = () => {
    updateLocalStorage({}, () => {
      this.setState({
        numbers: {},
        successText: '',
        infoText: '',
      });
    });
  };

  onSubmit = e => {
    const { input, numbers, repeatedList, insertedList } = this.state;
    if (!input) return; // if input is null/undefined/blank string, we return

    // split input on the basis of line(\n) first
    const arr = input
      .split('\n')
      .join(',')
      .split(',');

    // looping for logic
    for (let i = 0; i < arr.length; i++) {
      // if array includes '-', we split the numbers again and consider both numbers as range
      if (arr[i] && arr[i].includes('-')) {
        let ranges = arr[i].split('-').map(item => {
          return parseInt(item.trim()); // converts them to integer after trimming
        });

        // loop to check insertion of all the numbers in the range
        for (let j = ranges[0]; j <= ranges[1]; j++) {
          if (numbers[j] && !insertedList[j]) {
            // if number is present in original array, we insert number in repeatedList
            repeatedList[j] = 1;
          } else {
            /* if number is not present in original array, we insert number in insertedList 
              and in orignal list too.
             */
            insertedList[j] = 1;
            numbers[j] = 1;
          }
        }
      } else if (arr[i]) {
        arr[i] = parseInt(arr[i].trim()); // converts them to integer after trimming
        if (numbers[arr[i]] && !insertedList[arr[i]]) {
          // if number is present in original array, we insert number in repeatedList

          repeatedList[arr[i]] = 1;
        } else {
          /* if number is not present in original array, we insert number in insertedList 
              and in orignal list too.
             */
          insertedList[arr[i]] = 1;
          numbers[arr[i]] = 1;
        }
      }
    }

    let successText = '';
    let infoText = '';
    const repeatedArr = Object.keys(repeatedList); // converting Object to Array
    const insertedArr = Object.keys(insertedList); // converting Object to Array
    const repeatedArrLength = repeatedArr.length;
    const insertedArrLength = insertedArr.length;

    // simple logic for making info(for skipped numbers) and
    // success(for numbers that are newly inserted) text
    if (repeatedArrLength) {
      const lastNumber = repeatedArr.pop();
      if (repeatedArrLength === 1) {
        infoText = `The number ${lastNumber} was a duplicate and therefore skipped.`;
      } else {
        infoText = `The numbers ${repeatedArr.join(
          ', ',
        )} and ${lastNumber} were duplicates and therefore skipped.`;
      }
    }

    if (insertedArrLength) {
      const lastNumber = insertedArr.pop();
      let msg = '';

      if (insertedArrLength === 1) {
        msg = `The number ${lastNumber} was inserted successfully.`;
      } else {
        if (!repeatedArrLength) msg = 'All the ';
        else msg = 'The ';

        msg = `${msg} numbers (${insertedArr.join(
          ', ',
        )} and ${lastNumber}) were inserted successfully.`;
      }
      successText = msg;
    }

    // update or create the original array in localstorage
    updateLocalStorage(numbers, () => {
      this.setState({
        input: '',
        repeatedList: {},
        insertedList: {},
        numbers: numbers,
        infoText: infoText,
        successText: successText,
      });
    });
  };

  render() {
    const { classes } = this.props;
    const { infoText, successText, numbers } = this.state;

    return (
      <div className="App">
        <Grid
          container
          justify="center"
          direction="column"
          spacing={16}
          alignItems="center"
        >
          <Grid item>
            <TextField
              id="outlined-multiline-flexible"
              label="Add some numbers here"
              multiline
              name="input"
              rowsMax="4"
              value={this.state.input}
              onChange={this.handleChange('input')}
              className={classes.textField}
              margin="normal"
              helperText={`Numbers should be " , " or line separated and can be single or in range, ex-> 7000, 6000, 8000-8005`}
              variant="outlined"
            />
          </Grid>
          <Grid item>
            <Button
              variant="outlined"
              color="secondary"
              onClick={this.onSubmit}
              className={classes.button}
            >
              Submit
            </Button>
          </Grid>
          <Grid item className={classes.text}>
            <Grid container justify="center" spacing={8}>
              <Grid item>{successText ? 'Updated List' : 'Original List'}:</Grid>
              <Grid item className={classes.list}>
                <Typography
                  variant="subheading"
                  color="inherit"
                  className={classes.extraPadding}
                >
                  [{Object.keys(numbers).join(', ') || ' '}]
                </Typography>
              </Grid>
              <Grid item>
                <Tooltip title="Clear list">
                  <Clear
                    onClick={this.clearList}
                    className={classNames(classes.error, classes.pointer)}
                  />
                </Tooltip>
              </Grid>
            </Grid>
          </Grid>

          {infoText && (
            <Grid item xs={12} className={classes.text}>
              <Grid container justify="center" spacing={16}>
                <Grid item>
                  <Info className={classes.info} />
                </Grid>
                <Grid item xs={10} sm={6}>
                  <Typography variant="subheading" color="inherit">
                    {infoText}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
          {successText && (
            <Grid item xs={12} className={classes.text}>
              <Grid container justify="center" spacing={16}>
                <Grid item>
                  <CheckCircle className={classes.success} />
                </Grid>
                <Grid item xs={10} sm={8} md={6}>
                  <Typography variant="subheading" color="inherit">
                    {successText}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </div>
    );
  }
}

Task1.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Task1);
