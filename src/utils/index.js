module.exports = {
	/**
	 * return youtube video id from the youtube url received from the API
	 * @param  {String} url youtube url
	 * @return {String}     youtube id
	 */
	getYoutubeId(url) {
		if (!url) return;
		
		const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
		const match = url.match(regExp);

		if (match && match[2].length === 11) {
			return match[2];
		} else {
			return 'error';
		}
	},

	/**
	 * return the youtube url required to embed in html
	 * @param  {String} url youtube url
	 * @return {String}     the modified youtube url
	 */
	getYoutubeURI(url) {
		return `//www.youtube.com/embed/${this.getYoutubeId(url)}`;
	},

	/**
	 * returns day(date) and short month from the date string
	 * @param  {String} date string received from the API
	 * @return {String}      returns the modified string
	 */
	getDayMonth(date) {
		if (!date) return;
		const arr = date
			.split(',')
			.join(' ')
			.split(' ');
		arr.pop();
		return arr.join(' ');
	},

	/**
	 * returns year from the date string
	 * @param  {String} date string received from the API
	 * @return {String}      returns the modified string
	 */
	getYear(date) {
		if (!date) return;
		return date.split(' ').pop();
	},

	/**
	 * return genre of movies with space-around in pipe symbol
	 * @param  {String} genre the incoming string
	 * @return {String}       return the modified string
	 */
	getGenre(genre) {
		if (!genre) return;
		return genre.split('|').join(' | ');
	},
};
