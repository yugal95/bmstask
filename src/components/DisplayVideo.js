import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid,
  Typography,
  IconButton,
  Fade,
  Paper,
  Grow,
} from '@material-ui/core';
import {
  ThumbUpAlt,
  ThumbDownAlt,
  Today,
  HelpOutline,
} from '@material-ui/icons';
import UTIL from '../utils';

const MOVIE_DESCP = (
  <p>
    This is the movie description.
    <br /> Lorem ipsum dolor sit amet, donec nibh sodales, pellentesque velit
    justo lorem. Tellus dolor metus habitasse, tempor elit scelerisque aliquam,
    sollicitudin consectetuer.
  </p>
);

const styles = theme => ({
  root: {
    width: '100%',
    flexGrow: 1,
    marginRight: 0,
    marginLeft: 0,
    // margin: '1%',
    // padding: '1% 6%',
  },
  paper: {
    background: 'none',
    boxShadow: 'none',

    margin: '1%',
    padding: '1% 6%',
  },
  grow: {
    marginRight: '10px',
    // flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  link: {
    color: '#fff',
    textDecoration: 'none',
  },
  movieDetail: {
    color: '#fff',
    background: 'rgba(0,0,0,.2)',
  },
  descp: {
    overflow: 'hidden',
    height: 200,
    [theme.breakpoints.down('xs')]: {
      height: 'auto',
      maxHeight: 200,
    },
  },
  icon: {
    color: '#fff',
    padding: 0,
    opacity: 0.8,
  },
  iconDiv: {
    border: `3px solid`,
    borderRadius: '50%',
    opacity: 0.8,
    padding: '2%',
  },
  help: {
    fontSize: '44px',
    marginTop: '-3px',
    color: '#fff',
    padding: 0,
    [theme.breakpoints.down('xs')]: {
      marginTop: '-7px',
    },
    // opacity: 0.8,
  },
  video: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    [theme.breakpoints.down('xs')]: {
      padding: '0 !important',
      height: '350px',
    },
  },
  iframe: {
    width: '100%',
    height: '100%',
  },
  caption: {
    [theme.breakpoints.down('xs')]: {
      fontSize: '0.6em',
    },
  },
});

function DisplayVideo(props) {
  const { classes, movie } = props;
  const { TrailerURL } = movie;
  return (
    <Grow in={true}>
      <Paper elevation={4} className={classes.paper}>
        <Grid container spacing={32} className={classes.root}>
          <Grid
            item
            xs={12}
            sm={7}
            className={classes.video}
            style={{
              paddingTop: 0,
              paddingBottom: 0,
              paddingLeft: 0,
            }}
          >
            {true && (
              <iframe
                title="iframeideo"
                id="iframeideo"
                className={classes.iframe}
                src={UTIL.getYoutubeURI(TrailerURL)}
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              />
            )}
          </Grid>
          <Grid item xs={12} sm={5} className={classes.movieDetail}>
            <Grid container direction="column" spacing={8}>
              <Grid item xs={12}>
                <Typography variant="subheading" color="inherit">
                  {movie.EventTitle}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography color="inherit" variant="caption">
                  {movie.EventLanguage}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography color="inherit" variant="caption">
                  {UTIL.getGenre(movie.EventGenre)}
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <Grid container spacing={16}>
                  <Grid item>
                    <Grid container alignItems="center" spacing={8}>
                      <Grid item>
                        <IconButton className={classes.icon}>
                          <ThumbUpAlt />
                        </IconButton>
                      </Grid>
                      <Grid item>
                        <Grid container direction="column">
                          <Grid item>
                            <Typography color="inherit" variant="caption">
                              {movie.wtsPerc}%
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography variant="caption" color="inherit">
                              {movie.csCount} votes
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <Grid container alignItems="center" spacing={8}>
                      <Grid item>
                        <IconButton className={classes.icon}>
                          <Today />
                        </IconButton>
                      </Grid>

                      <Grid item>
                        <Grid container direction="column">
                          <Grid item>
                            <Typography color="inherit" variant="caption">
                              {UTIL.getDayMonth(movie.ShowDate)}
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography variant="caption" color="inherit">
                              {UTIL.getYear(movie.ShowDate)}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} className={classes.descp}>
                <Typography color="inherit" variant="caption">
                  {MOVIE_DESCP}
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={4}>
                    <Grid
                      container
                      direction="column"
                      alignItems="center"
                      spacing={8}
                    >
                      <Grid item className={classes.iconDiv}>
                        <IconButton className={classes.icon}>
                          <ThumbUpAlt />
                        </IconButton>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant="caption"
                          color="inherit"
                          align="center"
                          className={classes.caption}
                        >
                          WILL WATCH
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant="caption"
                          align="center"
                          color="inherit"
                        >
                          {`{${movie.wtsCount}}`}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item xs={4}>
                    <Grid
                      container
                      direction="column"
                      alignItems="center"
                      spacing={0}
                    >
                      <Grid item>
                        <IconButton className={classes.icon}>
                          <HelpOutline className={classes.help} />
                        </IconButton>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant="caption"
                          color="inherit"
                          align="center"
                          className={classes.caption}
                        >
                          MAYBE
                        </Typography>
                      </Grid>
                      <Grid item style={{ marginTop: '7px' }}>
                        <Typography
                          variant="caption"
                          align="center"
                          color="inherit"
                        >
                          {`{${movie.maybeCount}}`}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item xs={4}>
                    <Grid
                      container
                      direction="column"
                      alignItems="center"
                      spacing={8}
                    >
                      <Grid item className={classes.iconDiv}>
                        <IconButton className={classes.icon}>
                          <ThumbDownAlt />
                        </IconButton>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant="caption"
                          color="inherit"
                          align="center"
                          className={classes.caption}
                        >
                          WON'T WATCH
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography
                          variant="caption"
                          align="center"
                          color="inherit"
                        >
                          {`{${movie.dwtsCount}}`}
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </Grow>
  );
}

DisplayVideo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DisplayVideo);
