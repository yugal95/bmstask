import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';
import Modal from '@material-ui/core/Modal';

const styles = theme => ({
	loadingClass: {
		position: 'absolute',
		background: 'none',
		boxShadow: 'none',
		border: 'none',
		outline: 'none',
	},
});

function getModalStyle() {
	const top = 50;
	const left = 50;

	return {
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
	};
}

let Loader = props => {
	const { loading, classes } = props;
	if (!loading) {
		return <React.Fragment />;
	}
	return (
		<Modal open={loading}>
			<div style={getModalStyle()} className={classes.loadingClass}>
				<Fade in={loading} unmountOnExit>
					<CircularProgress thickness={5} size={50} />
				</Fade>
			</div>
		</Modal>
	);
};

Loader = withStyles(styles)(Loader);

export default Loader;
