import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';

import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const styles = {
  root: {
    width: '100%',
    flexGrow: 1,
  },
  grow: {
    marginRight: '10px',
    // flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  link: {
    color: '#fff',
    textDecoration: 'none',
  },
  about: {
    marginRight: 10
  }
};

const Links = [
  {
    id: 't1',
    key: 't1',
    label: 'Task1',
    to: '/task1',
  },
  {
    id: 't2',
    key: 't2',
    label: 'Task2',
    to: '/task2',
  },
];

const AboutLink = {
  id: 'about',
  key: 'about',
  label: 'About',
  to: 'https://github.com/lovingyugs',
};

function ButtonAppBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Grid container justify="space-between" alignItems="center">
          <Grid item>
            <Toolbar>
              <IconButton
                className={classes.menuButton}
                color="inherit"
                aria-label="Menu"
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.grow}>
                BMS Task
              </Typography>
              {Links.map((link, index) => {
                return (
                  <Link
                    key={link.key}
                    id={link.id}
                    to={{
                      pathname: link.to,
                      state: {
                        rerender: true,
                      },
                    }}
                    className={classes.link}
                  >
                    <Button color="inherit">{link.label}</Button>
                  </Link>
                );
              })}
            </Toolbar>
          </Grid>
          <Grid item xs={1} className={classes.about}>
            <Link
              target="_blank"
              key={AboutLink.key}
              id={AboutLink.id}
              to={{
                pathname: AboutLink.to,
                state: {
                  rerender: true,
                },
              }}
              className={classes.link}
            >
              <Button color="inherit">{AboutLink.label}</Button>
            </Link>
          </Grid>
        </Grid>
      </AppBar>
    </div>
  );
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonAppBar);
