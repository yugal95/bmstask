const express = require('express');
const router = express.Router();
const request = require('request');

const BMS_VIDEO_API =
  'https://in.bookmyshow.com/serv/getData?cmd=GETTRAILERS&mtype=cs';

function getMovies(req, res) {
  request(BMS_VIDEO_API, (err, response, body) => {
    if (err) {
      console.log(err);
      return res.status(400).json(err);
    }
    // console.log(res);
    return res.status(200).json(JSON.parse(body));
    // console.log(body.url);
    // console.log(body.explanation);
  });
}

router.get('/movies', getMovies);

module.exports = router;
