This project is hosted on [https://bmstask.herokuapp.com](https://bmstask.herokuapp.com).

## Installation and run

Go to the project directory and follow below commands-

### `npm install`

To install all the node dependencies

### `npm run build`

Builds the app for production to the *build* folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

### `npm start`

Runs the start script from package.json.

Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

### Deployment
Heroku is used for deployment.
