const express = require('express');
const path = require('path');
// const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const routes = require('./routes');
// const db_connect = require('./lib/db_connect')();

const app = express();

/*******************************************************
    MIDDLEWARES
********************************************************/

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// app.use(express.static(path.join(__dirname, '/build')));

/*******************************************************
    BASE API ENDPOINT
********************************************************/
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  next();
});

app.use('/api/v1', routes);
app.use(express.static(path.join(__dirname, '/build')));

/*******************************************************
    ERROR HANDLER FOR REQUESTS
********************************************************/

app.get('/', (req, res) => {
  res.json({
    status: 200,
    check: true,
  });
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  console.log('Not found');
  res.sendFile('./build/index.html', { root: __dirname });
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {},
  });
});

module.exports = app;
